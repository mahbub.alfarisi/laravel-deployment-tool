# Laravel Deployment Tool


This README provides instructions for using the deployment script to deploy a Laravel application on a Windows system with Apache and PHP.

## Overview

The deployment script automates the process of deploying a Laravel application by:

- [ ] Reading configuration settings from a file.
- [ ] Extracting the application files from a ZIP archive.
- [ ] Copying the application files to the Laravel directory.
- [ ] Setting permissions (optional).
- [ ] Restarting Apache (optional).
- [ ] Running Composer commands (optional).
- [ ] Running Artisan commands (optional).

## Tested Environment
The script has been tested successfully on:

Operating System: Windows 11
Web Server: Apache 2.4.59
PHP Version: PHP 8.3.8
Laravel Version: Laravel 10


## Configuration

The deployment script uses a configuration file (deploy_config.txt) to define various settings. Below is an example configuration file:

```
ZIP_FILE=deployment_package.zip
DEPLOY_DIR=C:\path\to\deployment
LARAVEL_DIR=C:\path\to\laravel
IS_SET_PERMISSION=true
IS_APACHE_RESTART=true
APACHE_SERVICE_NAME=Apache2.4
IS_COMPOSER_INSTALL=true
IS_COMPOSER_UPDATE=false
IS_ARTISAN_OPTIMIZE=true
APP_NAME=MyLaravelApp
APP_VERSION=1.0.0

```

### Configuration Options
- [ ] ZIP_FILE: The path to the ZIP file containing the deployment package.
- [ ] DEPLOY_DIR: The directory where the ZIP file will be extracted.
- [ ] LARAVEL_DIR: The target Laravel application directory.
- [ ] IS_SET_PERMISSION: Whether to set permissions on certain Laravel directories (true or false).
- [ ] IS_APACHE_RESTART: Whether to restart the Apache service after deployment (true or false).
- [ ] APACHE_SERVICE_NAME: The name of the Apache service to restart.
- [ ] IS_COMPOSER_INSTALL: Whether to run composer install after copying files (true or false).
- [ ] IS_COMPOSER_UPDATE: Whether to run composer update after copying files (true or false).
- [ ] IS_ARTISAN_OPTIMIZE: Whether to run php artisan optimize after copying files (true or false).
- [ ] APP_NAME: The name of the application (for display purposes).
- [ ] APP_VERSION: The version of the application (for display purposes).

***

## Usage
- [ ] Prepare the Deployment Package: Create a ZIP file containing the app, resources, config, and routes directories, along with any other necessary files.
- [ ] Create the Configuration File: Create a deploy_config.txt file with the appropriate settings.
- [ ] Run the Deployment Script: Execute the deploy.bat script.

### Prepare Deployment Package
The deployment_package.zip must contains all the updated or newly added files with exactly the same directory structure. For example, if the changes is on "routes/web.php" and "app/Http/Controllers/TestController.php", you must create a directory "routes" and put the updated "web.php" inside,

```
> routes
    > web.php
```

then create directory "app/Http/Contollers" and put the updated "TestController.php" inside.

```
> app
    > Http
        > Controlles
            > TestController.php
```

Finally, select all the directories and compress as "deployment_package.zip"

### Sample deploy_config.txt

```
ZIP_FILE=deployment_package.zip
DEPLOY_DIR=C:\path\to\deployment
LARAVEL_DIR=C:\path\to\laravel
BACKUP_DIR=C:\path\to\backup
IS_SET_PERMISSION=true
IS_APACHE_RESTART=true
APACHE_SERVICE_NAME=Apache2.4
IS_COMPOSER_INSTALL=true
IS_COMPOSER_UPDATE=false
IS_ARTISAN_OPTIMIZE=true
APP_NAME=MyLaravelApp
APP_VERSION=1.0.0

```

### Run Script

- [ ] Open command prompt as Administrator.
- [ ] Navigate to the directory where you placed the script.
- [ ] Type "deploy.bat" and press Enter.

***

## Notes

- [ ] Ensure that all the existing files and directories are already backed up.
- [ ] Ensure that all paths in the configuration file use double backslashes (\\) or single forward slashes (/) to avoid issues with path separators.
- [ ] The deploy.bat script and deploy_config.txt should be placed in the same directory as the deployment package ZIP file.

## Troubleshooting

- [ ] If you encounter issues with permissions, make sure the script is run with administrative privileges.
- [ ] Check the log output in the command prompt for any errors or warnings during the deployment process.
- [ ] Verify that the Apache service name in the configuration file matches the actual service name on your system.

By following these instructions, you can efficiently deploy your Laravel application while ensuring that existing files are backed up and the deployment process is automated.