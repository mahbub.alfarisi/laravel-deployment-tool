@echo off
set LOGFILE=%APP_NAME%-deploy.log

REM Redirect stdout and stderr to log file
echo Starting deployment at %DATE% %TIME%
echo Starting deployment at %DATE% %TIME% > %LOGFILE% 2>&1

REM Read configuration file
for /f "tokens=1,2 delims==" %%i in (deploy_config.txt) do (
    set "%%i=%%j"
)

REM Unzip the application
echo.
echo Extracting %ZIP_FILE% to %DEPLOY_DIR%...
echo ______________________________________________
echo.
echo Extracting %ZIP_FILE% to %DEPLOY_DIR%... >> %LOGFILE%
echo ______________________________________________ >> %LOGFILE%
echo. >> %LOGFILE%
powershell -Command "Expand-Archive -Path '%ZIP_FILE%' -DestinationPath '%DEPLOY_DIR%' -Force" >> %LOGFILE% 2>&1

REM Copy directories to the Laravel application directory
echo Copying files to the Laravel application directory...
echo Copying files to the Laravel application directory... >> %LOGFILE%
robocopy "%DEPLOY_DIR%" "%LARAVEL_DIR%" /E /XO /XC /XN /COPYALL /IS /IT /XF deploy.bat deploy_config.txt deployment_package.zip >> %LOGFILE% 2>&1

REM Set permissions (optional, adjust as needed)
if /i "%IS_SET_PERMISSION%"=="true" (
    echo.
    echo Setting permissions...
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Setting permissions... >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
    icacls "%LARAVEL_DIR%\storage" /grant Everyone:F /T >> %LOGFILE% 2>&1
    icacls "%LARAVEL_DIR%\bootstrap\cache" /grant Everyone:F /T >> %LOGFILE% 2>&1
) else (
    echo.
    echo Set permissions skipped.
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Set permissions skipped. >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
)

REM Restart Apache if needed
if /i "%IS_APACHE_RESTART%"=="true" (
    echo.
    echo Restarting Apache %APACHE_SERVICE_NAME% ...
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Restarting Apache %APACHE_SERVICE_NAME% ... >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
    net stop "%APACHE_SERVICE_NAME%" >> %LOGFILE% 2>&1
    net start "%APACHE_SERVICE_NAME%" >> %LOGFILE% 2>&1
) else (
    echo.
    echo Restart Apache skipped.
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Restart Apache skipped. >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
)

REM Run Composer install if needed
if /i "%IS_COMPOSER_INSTALL%"=="true" (
    echo.
    echo Installing Composer dependencies...
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Installing Composer dependencies... >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
    cd "%LARAVEL_DIR%"
    call composer install --optimize-autoloader --no-dev >> %LOGFILE% 2>&1 || (
        echo.
        echo Composer install failed. Check the logs for details.
        echo ______________________________________________
        echo.
        echo. >> %LOGFILE%
        echo Composer install failed. Check the logs for details. >> %LOGFILE%
        echo ______________________________________________ >> %LOGFILE%
        echo. >> %LOGFILE%
        goto end
    )
) else (
    echo.
    echo Install Composer skipped.
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Install Composer skipped. >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
)

REM Run Composer update if needed
if /i "%IS_COMPOSER_UPDATE%"=="true" (
    echo.
    echo Updating Composer dependencies...
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Updating Composer dependencies... >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
    cd "%LARAVEL_DIR%"
    call composer update --optimize-autoloader --no-dev >> %LOGFILE% 2>&1 || (
        echo.
        echo Composer update failed. Check the logs for details.
        echo ______________________________________________
        echo.
        echo. >> %LOGFILE%
        echo Composer update failed. Check the logs for details. >> %LOGFILE%
        echo ______________________________________________ >> %LOGFILE%
        echo. >> %LOGFILE%
        goto end
    )
) else (
    echo.
    echo Update Composer skipped.
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Update Composer skipped. >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
)

REM Run PHP Artisan optimize (if needed)
if /i "%IS_ARTISAN_OPTIMIZE%"=="true" (
    echo.
    echo Running Artisan optimization...
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Running Artisan optimization... >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
    cd "%LARAVEL_DIR%"
    php artisan optimize >> %LOGFILE% 2>&1
) else (
    echo.
    echo Artisan optimization skipped.
    echo ______________________________________________
    echo.
    echo. >> %LOGFILE%
    echo Artisan optimization skipped. >> %LOGFILE%
    echo ______________________________________________ >> %LOGFILE%
    echo. >> %LOGFILE%
)

cd "%DEPLOY_DIR%"
echo Deployment completed.
echo %APP_NAME% - %APP_VERSION% Successfully Deployed at %DATE% %TIME%.
echo Deployment completed. >> %LOGFILE%
echo %APP_NAME% - %APP_VERSION% Successfully Deployed at %DATE% %TIME%. >> %LOGFILE%
pause
